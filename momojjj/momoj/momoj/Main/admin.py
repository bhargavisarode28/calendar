from django.contrib import admin

from .models import Event

admin.site.site_header = 'RAIT CALENDAR ADMIN PANEL'



admin.site.register(Event)