import uuid


from django.db import models
from django.contrib.auth.models import User

class Event(models.Model):
    # product_id = models.BigAutoField(primary_key=True)
    # product_id2 = models.CharField(max_length=100, default='')
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=100)
    time = models.DateTimeField()
    desc = models.CharField(max_length=10000000000000)

    class Meta:
         db_table = "Event"