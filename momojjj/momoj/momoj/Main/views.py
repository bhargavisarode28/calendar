


from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Event
import json




def index(request):
    event_list = Event.objects.all()
    all_events = []
    print(event_list)
    for event in event_list: 
        if(event.time):
            event.time = event.time.timestamp() * 1000
            sample_event = {
                "name": event.name,
                "time": event.time,
                "desc": event.desc
            }
            all_events.append((sample_event))
    return render(request, 'index.html', {'event_list':all_events})

